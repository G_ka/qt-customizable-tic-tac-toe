#include <mainWindow.h>

#ifndef VICTORYCHECK_H
#define VICTORYCHECK_H

bool victoryCheckX(int origX, int origY)
{
    int sum=0, x=origX, y=origY;

    while(x<g_gridSize && g_boxValue[x][y] == 1)
    {
        ++x;
    }

    --x;

    while(x<g_gridSize && g_boxValue[x][y] == 1)
    {
        sum+=1;
        --x;
    }

    if(sum>=g_victorySize)
    {
        return true;
    }

    x=origX;
    sum=0;

    while(y<g_gridSize && g_boxValue[x][y] == 1)
    {
        ++y;
    }

    --y;

    while(y<g_gridSize && g_boxValue[x][y] == 1)
    {
        sum+=1;
        --y;
    }

    if(sum>=g_victorySize)
    {
        return true;
    }

    x=origX;
    y=origY;
    sum=0;

    while (x<g_gridSize && y<g_gridSize && g_boxValue[x][y] == 1)
    {
        ++x;
        ++y;
    }

    --x;
    --y;

    while (x<g_gridSize && y<g_gridSize && g_boxValue[x][y] == 1)
    {
        sum+=1;
        --x;
        --y;
    }

    if(sum>=g_victorySize)
    {
        return true;
    }

    x=origX;
    y=origY;
    sum=0;

    while (x<g_gridSize && y<g_gridSize && g_boxValue[x][y] == 1)
    {
        --x;
        ++y;
    }

    ++x;
    --y;

    while (x<g_gridSize && y<g_gridSize && g_boxValue[x][y] == 1)
    {
        sum+=1;
        ++x;
        --y;
    }

    if(sum>=g_victorySize)
    {
        return true;
    }

    return 0;
}

bool victoryCheckO(int origX, int origY)
{
    int sum=0, x=origX, y=origY;

    while(x<g_gridSize && g_boxValue[x][y] == 2)
    {
        ++x;
    }

    --x;

    while(x<g_gridSize && g_boxValue[x][y] == 2)
    {
        sum+=1;
        --x;
    }

    if(sum>=g_victorySize)
    {
        return true;
    }

    x=origX;
    sum=0;

    while(y<g_gridSize && g_boxValue[x][y] == 2)
    {
        ++y;
    }

    --y;

    while(y<g_gridSize && g_boxValue[x][y] == 2)
    {
        sum+=1;
        --y;
    }

    if(sum>=g_victorySize)
    {
        return true;
    }

    x=origX;
    y=origY;
    sum=0;

    while (x<g_gridSize && y<g_gridSize && g_boxValue[x][y] == 2)
    {
        ++x;
        ++y;
    }

    --x;
    --y;

    while (x<g_gridSize && y<g_gridSize && g_boxValue[x][y] == 2)
    {
        sum+=1;
        --x;
        --y;
    }

    if(sum>=g_victorySize)
    {
        return true;
    }

    x=origX;
    y=origY;
    sum=0;

    while (x<g_gridSize && y<g_gridSize && g_boxValue[x][y] == 2)
    {
        --x;
        ++y;
    }

    ++x;
    --y;

    while (x<g_gridSize && y<g_gridSize && g_boxValue[x][y] == 2)
    {
        sum+=1;
        ++x;
        --y;
    }

    if(sum>=g_victorySize)
    {
        return true;
    }


    return 0;
}

#endif // VICTORYCHECK_H

