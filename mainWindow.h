//mainWindow.h

#include <QLayout>
#include <QApplication>
#include <QPushButton>
#include <QLabel>

extern int g_gridSize; //size of the Tic Tac Toe game
extern int g_victorySize; //number of symbols required to win
extern int g_boxValue[30][30];
extern char winner;

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

class mainWindow : public QWidget
{

    Q_OBJECT

    public:
    mainWindow();

    private:
    QPushButton *mW_button;
    QGridLayout *mW_layout;
    QLabel *mW_label;

};

#endif // MAINWINDOW_H

