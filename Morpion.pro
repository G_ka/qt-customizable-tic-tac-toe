SOURCES += \
        main.cpp \
    mainWindow.cpp \
    askWindow.cpp \
    victoryWindow.cpp

QT += widgets

HEADERS += \
    mainWindow.h \
    askWindow.h \
    myButton.h \
    victoryCheck.h \
    victoryWindow.h
