//myButton.h

#include <QPushButton>
#include <mainWindow.h>

#ifndef MYBUTTON_H
#define MYBUTTON_H

class myButton : public QPushButton
{
    Q_OBJECT

public:
    myButton(QWidget* parent = Q_NULLPTR) {}

public slots:
    void pushed();

private:
    int boxX(std::string s);
    int boxY(std::string s);
};

#endif // MYBUTTON_H
