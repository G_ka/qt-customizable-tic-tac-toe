// askWindow.h

#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QMessageBox>
#include <QLCDNumber>
#include <QSlider>
#include <QLayout>
#include <QLabel>

#ifndef ASKWINDOW_H
#define ASKWINDOW_H

class askWindow : public QDialog // QDialog inheritance to allow "exec" method
{
    Q_OBJECT

    public:
    askWindow();

    public slots:
    void gridSliderToVar(int value);
    void victorySliderToVar(int value);

    private:
    QPushButton *aW_button;
    QLCDNumber *aW_lcd_victorySize;
    QLCDNumber *aW_lcd_gridSize;
    QSlider *aW_slider_gridSize;
    QSlider *aW_slider_victorySize;
    QGridLayout *aW_layout;
    QLabel *aW_label_victorySize;
    QLabel *aW_label_gridSize;
    QFrame* separator = new QFrame();
};

#endif // ASKWINDOW_H
