My first attempt to create a program. A Qt C++ Tic Tac Toe.
Features :
- User-defined size, up to a 30*30 grid
- User-defined "victory size", the number of aligned symbols required to win
- Victory check
- No IA, two players required 
