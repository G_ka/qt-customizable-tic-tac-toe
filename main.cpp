#include "mainWindow.h"
#include "askWindow.h"
#include "victoryWindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    askWindow askWindow;
    askWindow.exec(); //exec() instead of show() to make the program wait before running the mainWindow

    mainWindow mainWindow;
    mainWindow.show();

    return app.exec();
}
