// askWindow.cpp

#include <mainWindow.h>
#include <askWindow.h>

int g_gridSize=3;
int g_victorySize=3;

askWindow::askWindow() : QDialog () //Ask the user for grid size and number of aligned symbols required to win
{

    aW_layout = new QGridLayout();
    this->setLayout(aW_layout);

    ///Grid size

    aW_label_gridSize = new QLabel(this);
    aW_label_gridSize->setText("                            Grid Size");
    aW_layout->addWidget(aW_label_gridSize,1, 0);

    aW_slider_gridSize = new QSlider(Qt::Horizontal, this);
    aW_slider_gridSize->setGeometry(10, 60, 150, 20);
    aW_slider_gridSize->setSingleStep(5);
    aW_slider_gridSize->setPageStep(1);
    aW_slider_gridSize->setMinimum(3);
    aW_slider_gridSize->setMaximum(30);
    aW_layout->addWidget(aW_slider_gridSize, 2, 0);

    aW_lcd_gridSize = new QLCDNumber(this);
    aW_lcd_gridSize->setSegmentStyle(QLCDNumber::Flat);
    aW_lcd_gridSize->move(50, 20);
    aW_lcd_gridSize->display(3);
    aW_layout->addWidget(aW_lcd_gridSize, 3, 0);


    separator = new QFrame(this);
    separator->setFrameShape(QFrame::HLine);
    separator->setLineWidth(50);
    aW_layout->addWidget(separator,4,0);

    //Number of aligned symbols required to win

    aW_label_victorySize = new QLabel(this);
    aW_label_victorySize->setText("Number of aligned symbols required to win");
    aW_layout->addWidget(aW_label_victorySize, 5, 0);

    aW_slider_victorySize = new QSlider(Qt::Horizontal, this);
    aW_slider_victorySize->setGeometry(10, 60, 150, 20);
    aW_slider_victorySize->setSingleStep(5);
    aW_slider_victorySize->setPageStep(1);
    aW_slider_victorySize->setMinimum(3);
    aW_slider_victorySize->setMaximum(30);
    aW_layout->addWidget(aW_slider_victorySize, 6, 0);

    aW_lcd_victorySize = new QLCDNumber(this);
    aW_lcd_victorySize->setSegmentStyle(QLCDNumber::Flat);
    aW_lcd_victorySize->move(50, 20);
    aW_lcd_victorySize->display(3);
    aW_layout->addWidget(aW_lcd_victorySize, 7, 0);


    aW_button = new QPushButton("Validate", this);
    aW_layout->addWidget(aW_button, 0, 0);


    QObject::connect(aW_slider_gridSize, SIGNAL(valueChanged(int)), this, SLOT(gridSliderToVar(int))) ;
    QObject::connect(aW_slider_victorySize, SIGNAL(valueChanged(int)), this, SLOT(victorySliderToVar(int))) ;
    QObject::connect(aW_slider_gridSize, SIGNAL(valueChanged(int)), aW_lcd_gridSize, SLOT(display(int))) ;
    QObject::connect(aW_slider_victorySize, SIGNAL(valueChanged(int)), aW_lcd_victorySize, SLOT(display(int))) ;
    QObject::connect(aW_button, SIGNAL(clicked()), this, SLOT(close())) ;
}

void askWindow::gridSliderToVar(int value) //Save slider to var
{
    g_gridSize=value;
}

void askWindow::victorySliderToVar(int value) //Save slider to var
{
    g_victorySize=value;
}
