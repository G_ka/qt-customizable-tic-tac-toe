#include <mainWindow.h>
#include <askWindow.h>
#include <myButton.h>
#include <victoryCheck.h>
#include <victoryWindow.h>

static int g_turn;
int g_boxValue[30][30]={{0}};

mainWindow::mainWindow() : QWidget ()
{
    mW_layout = new QGridLayout();
    this->setLayout(mW_layout);

    // Button construction

    for(int j=0; j<g_gridSize; ++j)
    {
        for (int k = 0; k < g_gridSize; ++k)
        {
            myButton* new_button = new myButton(this);
            new_button ->setCursor(Qt::PointingHandCursor);
            new_button->setFixedHeight(30);
            new_button->setFixedWidth(30);
            mW_layout->addWidget(new_button , j, k);
            QObject::connect(new_button , SIGNAL(clicked()), new_button , SLOT(pushed()));
            new_button->setAccessibleName(QString::number(j) + "|" + QString::number(k)); //To know which button was pushed
        }
    }
}

void myButton::pushed()
{
    bool Xwinner=false, Owinner=false;
    std::string name = accessibleName().toStdString();
    if(text()!='X' && text()!='O') //if the box is empty
    {


    //Store buttons content to arrays, this will allow to check for victory.

    if (g_turn % 2 == 0)
    {
        setText("X");
        g_boxValue[boxX(name)][boxY(name)]=1;
        Xwinner = victoryCheckX(boxX(name), boxY(name));
    }
    else
    {
        setText("O");
        g_boxValue[boxX(name)][boxY(name)]=2;
        Owinner = victoryCheckO(boxX(name), boxY(name));
    }
    ++g_turn;
    }
    if (Xwinner)
    {
        victoryWindow victoryWindow('X');
        victoryWindow.exec();

    }else if (Owinner)
    {
        victoryWindow victoryWindow('O');
        victoryWindow.exec();
    }
}

int myButton::boxX(std::string s) //find button row from its accessible name
{
    std::string::size_type pos = s.find('|');
    if (pos != std::string::npos)
    {
        s = s.substr(0, pos);
        int x = stoi(s);
        return x;
    }
    else
    {
        int x = stoi(s);
        return x;
    }
}

int myButton::boxY(std::string s) //find button column from its accessible name
{
    s = s.substr(s.find("|") + 1);
    int y = stoi(s);
    return y;
}
