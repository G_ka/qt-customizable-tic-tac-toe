#ifndef VICTORYWINDOW_H
#define VICTORYWINDOW_H

#include <QLabel>
#include <QDialog>
#include <mainWindow.h>

class victoryWindow : public QDialog
{
    Q_OBJECT

public:
    victoryWindow(char winner);

private:
    QLabel *vW_label;

};
#endif // VICTORYWINDOW_H
